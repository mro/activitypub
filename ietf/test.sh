#!/bin/sh
cat <<EOF
This is the sample with test-data from 

https://tools.ietf.org/id/draft-cavage-http-signatures-12.html#rfc.appendix.C

EOF

base64 --version >/dev/null || { echo "please install $ sudo apt-get install coreutils" && exit 1; }
jq --version >/dev/null     || { echo "please install $ sudo apt-get install jq" && exit 1; }
openssl version >/dev/null  || { echo "please install $ sudo apt-get install openssl" && exit 1; }

cd "$(dirname "${0}")" || exit 1


printf "%s: %s\n%s: %s\n%s: %s" \
  "(request-target)" "post /foo?param=value&pet=dog" \
  "host" "example.com" \
  "date" "Sun, 05 Jan 2014 21:31:40 GMT" \
> "/tmp/headers.txt"

cat "/tmp/headers.txt"
echo ""
echo ""
echo "qdx+H7PHHDZgy4y/Ahn9Tny9V3GP6YgBPyUXMmoxWtLbHpUnXS2mg2+SbrQDMCJypxBLSPQR2aAjn7ndmw2iicw3HMbe8VfEdKFYRqzic+efkb3nndiv/x1xSHDJWeSWkx3ButlYSuBskLu6kd9Fswtemr3lgdDEmn04swr2Os0="

echo ""
openssl dgst -sha256 -sign "private.pem" -binary "/tmp/headers.txt" \
| base64 -w 0

echo "qdx+H7PHHDZgy4y/Ahn9Tny9V3GP6YgBPyUXMmoxWtLbHpUnXS2mg2+SbrQDMCJypxBLSPQR2aAjn7ndmw2iicw3HMbe8VfEdKFYRqzic+efkb3nndiv/x1xSHDJWeSWkx3ButlYSuBskLu6kd9Fswtemr3lgdDEmn04swr2Os0=" \
| base64 --decode \
> "/tmp/sig.bin"

echo ""
echo ""
openssl dgst \
  -sha256 \
  -verify "public.pem" \
  -signature "/tmp/sig.bin" \
  "/tmp/headers.txt"

