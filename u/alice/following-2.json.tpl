{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1"
  ],
  "id": "${id}following-2.json",
  "type": "OrderedCollectionPage",
  "totalItems": 215,
  "orderedItems": [
    "https://gnusocial.net/index.php/user/222853",
    "https://gnusocial.net/index.php/user/222530",
    "https://gnusocial.net/index.php/user/222426",
    "https://gnusocial.net/index.php/user/222305",
    "https://gnusocial.net/index.php/user/222288",
    "https://gnusocial.net/index.php/user/222240",
    "https://gnusocial.net/index.php/user/222220"
  ],
  "partOf": "${id}following.json",
  "next": "${id}following-1.json"
}
