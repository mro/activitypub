{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1"
  ],
  "id": "${id}followers-4.json",
  "type": "OrderedCollectionPage",
  "totalItems": 215,
  "orderedItems": [
    "https://gnusocial.net/index.php/user/222853",
    "https://gnusocial.net/index.php/user/222530",
    "https://gnusocial.net/index.php/user/222426",
    "https://gnusocial.net/index.php/user/222305",
    "https://gnusocial.net/index.php/user/222288",
    "https://gnusocial.net/index.php/user/222240",
    "https://gnusocial.net/index.php/user/222220"
  ],
  "partOf": "${id}followers.json",
  "next": "${id}followers-3.json"
}
