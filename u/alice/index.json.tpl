{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1"
  ],
  "id": "${id}",
  "type": "Person",
  "inbox": "${id}inbox.json",
  "outbox": "${id}outbox.json",
  "followers": "${id}followers.json",
  "following": "${id}following.json",
  "liked": "${id}liked.json",
  "preferredUsername": "alice",
  "name": "Alice from Exampleland",
  "publicKey": {
    "id": "${id}#main-key",
    "owner": "${id}",
    "publicKeyPem": ${pub_pem}
  },
  "attachment": [
    {
      "name": "Generator",
      "value": "mro.name/activitypub",
      "type": "PropertyValue"
    }
  ]
}
