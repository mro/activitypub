#!/bin/sh
base64 --version >/dev/null || { echo "please install $ sudo apt-get install coreutils" && exit 1; }
jq --version >/dev/null     || { echo "please install $ sudo apt-get install jq" && exit 1; }
openssl version >/dev/null  || { echo "please install $ sudo apt-get install openssl" && exit 1; }

cd "$(dirname "${0}")" || exit 1

readonly ptsrc='https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop'
curl -LO \
  --time-cond http-signature.json \
  "${ptsrc}/server/tests/fixtures/ap-json/mastodon/http-signature.json"
#curl -LO \
#  --time-cond keys.json \
#  "${ptsrc}/server/tests/fixtures/ap-json/peertube/keys.json"
curl -LO \
  --time-cond public-key.json \
  "${ptsrc}/server/tests/fixtures/ap-json/mastodon/public-key.json"

jq -r .publicKey  public-key.json > public.pem
#jq -r .privateKey keys.json > private.pem

jq -r .headers.signature http-signature.json \
| sed 's/.\+signature="\([^"]\+\)"/\1/' \
| base64 --decode \
> "/tmp/sig.bin"

readonly to_host="$(jq -r .headers.host http-signature.json)"
readonly now="$(jq -r .headers.date http-signature.json)"
readonly dgst="$(jq -r .headers.digest http-signature.json)"

printf "%s: %s\n%s: %s\n%s: %s\n%s: %s\n%s: %s" \
  "(request-target)" "post /accounts/ronan/inbox" \
  "host" "${to_host}" \
  "date" "${now}" \
  "digest" "${dgst}" \
  "content-type" "application/activity+json" \
> "/tmp/payload"

cat /tmp/payload
echo ""
echo ""

# https://mastodoncn.netlify.app/en/spec/security/#ld-verify
openssl dgst \
  -sha256 \
  -verify "public.pem" \
  -signature "/tmp/sig.bin" \
  "/tmp/payload"

