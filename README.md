```

    _        _   _       _ _         ____        _     
   / \   ___| |_(_)_   _(_) |_ _   _|  _ \ _   _| |__  
  / _ \ / __| __| \ \ / / | __| | | | |_) | | | | '_ \ 
 / ___ \ (__| |_| |\ V /| | |_| |_| |  __/| |_| | |_) |
/_/   \_\___|\__|_| \_/ |_|\__|\__, |_|    \__,_|_.__/ 
                               |___/                   

Copyright (C) 2022-2022 Marcus Rohrmoser, http://mro.name/activitypub
```

# ActivityPub Mould

Add some running real world examples to https://www.w3.org/TR/activitypub/

## Design Goals

| Quality         | very good | good | normal | irrelevant |
|-----------------|:---------:|:----:|:------:|:----------:|
| Functionality   |           |      |    ×   |            |
| Reliability     |     ×     |      |        |            |
| Usability       |     ×     |      |        |            |
| Efficiency      |           |      |    ×   |            |
| Changeability   |           |  ×   |        |            |
| Portability     |           |      |    ×   |            |

