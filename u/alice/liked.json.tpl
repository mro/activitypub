{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1"
  ],
  "id": "${id}liked.json",
  "type": "OrderedCollection",
  "totalItems": 369,
  "orderedItems": [
    {
      "created": "2022-06-01 22:03:58",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11926770",
        "type": "Note",
        "published": "2022-06-01T21:05:43Z",
        "url": "https://gnusocial.net/notice/11926770",
        "attributedTo": "https://gnusocial.net/index.php/user/10503",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/10503/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6912505",
        "conversationUrl": "https://gnusocial.net/conversation/6912505#notice-11926770",
        "content": "Por favor, si puedes, haz una donación periódica o puntual.<br />\n<br />\n<br />\nMas info en: <a href=\"https://elbinario.net/limosna-2/\" title=\"https://elbinario.net/limosna-2/\" rel=\"nofollow noreferrer\" class=\"attachment\">https://elbinario.net/limosna-2/</a> o preguntar al @<a href=\"${id}\" class=\"h-card u-url p-nickname mention\">administrator</a> <a href=\"https://gnusocial.net/url/9580046\" title=\"https://gnusocial.net/url/9580046\" rel=\"nofollow noreferrer\" class=\"attachment\">https://gnusocial.net/url/9580046</a>",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "image/webp",
            "url": "https://gnusocial.net/attachment/7325196f7bad0b7d6348871872f47e0a91be03bd75ceeb8aa13b6cf32802e9dc/view",
            "size": 226066,
            "name": "Editar Boteo GNU SOCIAL.webp",
            "meta": {
              "width": "1200",
              "height": "1200"
            }
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ]
      }
    },
    {
      "created": "2022-06-01 20:06:05",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11921261",
        "type": "Note",
        "published": "2022-06-01T08:52:02Z",
        "url": "https://hispagatos.space/@humo/108401362250909719",
        "attributedTo": "https://hispagatos.space/users/humo",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://gnusocial.net/index.php/user/180958",
          "https://masto.rocks/users/Xana"
        ],
        "cc": [
          "https://gnusocial.net/user/204427/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6909422",
        "conversationUrl": "https://gnusocial.net/conversation/6909422#notice-11921261",
        "content": "<p><a href=\"https://gnusocial.net/colegota\" class=\"u-url mention\">@colegota</a> <a href=\"https://masto.rocks/@Xana\" class=\"u-url mention\">@Xana</a> por no hablar del.derecho a la intimidad y el secreto en las telecomunicaciones y como vamos a ser gobernados y controlados laboralmente y socialmente por algoritmos e IA's.<br />Les vamos a dejar un mundo de mierda a nuestros hijos.</p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://gnusocial.net/index.php/user/180958",
            "name": "colegota@gnusocial.net"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://masto.rocks/users/Xana",
            "name": "Xana@masto.rocks"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11920861"
      }
    },
    {
      "created": "2022-06-01 20:02:04",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11925198",
        "type": "Note",
        "published": "2022-06-01T17:30:56Z",
        "url": "https://social.linux.pizza/@selea/108403402388667166",
        "attributedTo": "https://social.linux.pizza/users/selea",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://mcd.dk/users/rune"
        ],
        "cc": [
          "https://gnusocial.net/user/75485/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6911554",
        "conversationUrl": "https://gnusocial.net/conversation/6911554#notice-11925198",
        "content": "<p><a href=\"https://mcd.dk/@rune\" class=\"u-url mention\">@rune</a> </p><p>gmail is the biggest source of spam</p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mcd.dk/users/rune",
            "name": "rune@mcd.dk"
          }
        ],
        "inReplyTo": "https://mcd.dk/@rune/108403350994053474"
      }
    },
    {
      "created": "2022-06-01 08:35:01",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11920861",
        "type": "Note",
        "published": "2022-06-01T07:56:00Z",
        "url": "https://gnusocial.net/notice/11920861",
        "attributedTo": "https://gnusocial.net/index.php/user/180958",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://masto.rocks/users/Xana"
        ],
        "cc": [
          "https://gnusocial.net/user/180958/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6909422",
        "conversationUrl": "https://gnusocial.net/conversation/6909422#notice-11920861",
        "content": "Que a la sociedad le falta una reflexión sobre la tecnología de la vigilancia y las consecuencias que tendrá el que (muy poderosas) empresas, mafias y gobiernos, sepan cuáles son las debilidades y fortalezas de cada persona y sus seres queridos.<br />\n<br />\nY mientras no se haga esa reflexión, que los colegios o administraciónes hagan lo mismo con la infancia, no se verá como un problema.<br />\nPorque somos parte del problema.",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://masto.rocks/users/Xana",
            "name": "Xana@masto.rocks"
          }
        ]
      }
    },
    {
      "created": "2022-06-01 08:28:43",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11920337",
        "type": "Note",
        "published": "2022-06-01T06:40:35Z",
        "url": "https://gnusocial.net/notice/11920337",
        "attributedTo": "https://gnusocial.net/index.php/user/222240",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/222240/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6906287",
        "conversationUrl": "https://gnusocial.net/conversation/6906287#notice-11920337",
        "content": "@<a href=\"${id}\" class=\"h-card u-url p-nickname mention\" title=\"admin de gnusocial.net\">administrator</a> thank you for you help! I'm happy you discovered a new bug. For now i'm trying with session which looks very interesting",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11917631"
      }
    },
    {
      "created": "2022-05-31 17:37:10",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11915673",
        "type": "Note",
        "published": "2022-05-31T16:02:01Z",
        "url": "https://gnusocial.net/notice/11915673",
        "attributedTo": "176220",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/176220/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6905837",
        "conversationUrl": "https://gnusocial.net/conversation/6905837#notice-11915673",
        "content": "Thank you. <br />\nAnd again: gnusocial is like a mine, the deeper you dig the more precious jewels (aka glitches) you may find :-)",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11915586"
      }
    },
    {
      "created": "2022-05-25 22:21:14",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11867423",
        "type": "Note",
        "published": "2022-05-25T21:55:38Z",
        "url": "https://gnusocial.net/notice/11867423",
        "attributedTo": "https://gnusocial.net/index.php/user/176220",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/176220/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6882013",
        "conversationUrl": "https://gnusocial.net/conversation/6882013#notice-11867423",
        "content": "Hey!! Happy birthday, dear gnusocial.net and a bouquet of praise to @<a href=\"${id}\" class=\"h-card u-url p-nickname mention\">administrator</a> who keeps it such a hospitable place. Thank you all! Onwards to the next six years.",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11866616"
      }
    },
    {
      "created": "2022-05-25 20:17:44",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11866789",
        "type": "Note",
        "published": "2022-05-25T20:16:48Z",
        "url": "https://gnusocial.net/notice/11866789",
        "attributedTo": "https://gnusocial.net/index.php/user/30670",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/30670/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6882013",
        "conversationUrl": "https://gnusocial.net/conversation/6882013#notice-11866789",
        "content": "Felicidades y gracias por el trabajo :)<br />\nSe está tan cómodo acá que ni cuenta del tiempo que estamos, en mi caso 2 años después de esa fecha 😮",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11866616"
      }
    },
    {
      "created": "2022-05-19 09:25:43",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11811006",
        "type": "Note",
        "published": "2022-05-19T09:12:17Z",
        "url": "https://gnusocial.net/notice/11811006",
        "attributedTo": "https://gnusocial.net/index.php/user/180958",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/180958/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6855888",
        "conversationUrl": "https://gnusocial.net/conversation/6855888#notice-11811006",
        "content": "Con esto del aniversario del #<span class=\"tag\"><a href=\"https://gnusocial.net/tag/fediverso\" rel=\"tag\">Fediverso</a></span> me he vuelto a acordar de que tenía pendiente de revisar un hilo en FotoLibre que dio paso a mi entrada en la entonces identi.ca<br />\n<br />\n<a href=\"http://comunidad.fotolibre.net/index.php/topic,5516.msg59011.html#msg59011\" title=\"http://comunidad.fotolibre.net/index.php/topic,5516.msg59011.html#msg59011\" rel=\"nofollow noreferrer\" class=\"attachment\">http://comunidad.fotolibre.net/index.php/topic,5516.msg59011.html#msg59011</a><br />\n<br />\nEso sí, el paso del tiempo es muy cruel. Eso era 2010. Espero que mis amistades no lean mis comentarios en ese hilo, tanto al comentar que me había registrado en identi.ca, como los anteriores cuando al comienzo del hilo se pregunta por Twitter. ;)<br />\n<br />\nY otra cosa que me llama la atención, es que nada más entrar, ya estábamos hablando de grupos.<br />\n¡Cuánto se ha perdido en el Fediverso por no mantener los grupos! El mejor invento del milenio.",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "text/html; charset=UTF-8",
            "url": "http://comunidad.fotolibre.net/index.php/topic,5516.msg59011.html#msg59011",
            "size": 0,
            "name": "Hay twitteros por aquí?"
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "fediverso",
            "url": "https://gnusocial.net/tag/fediverso"
          }
        ]
      }
    },
    {
      "created": "2022-05-19 08:45:21",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11806310",
        "type": "Note",
        "published": "2022-05-18T15:49:02Z",
        "url": "https://social.diekershoff.de/display/f3ad7b1c-2262-80c1-6133-1fa304685401",
        "attributedTo": "https://social.diekershoff.de/profile/tobias",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/1360/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6853579",
        "conversationUrl": "https://gnusocial.net/conversation/6853579#notice-11806310",
        "content": "Happy 14th Birthday dear <a href=\"https://social.diekershoff.de/search?tag=fediverse\" class=\"mention hashtag\" rel=\"tag\">#<span>fediverse</span></a> <br /><a href=\"https://wayback.archive.org/web/20130615111045/http://identi.ca/notice/1\" rel=\"noreferrer\">wayback.archive.org/web/201306…</a><br />",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "fediverse",
            "url": "https://gnusocial.net/tag/fediverse"
          }
        ]
      }
    },
    {
      "created": "2022-05-19 08:24:38",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11810471",
        "type": "Note",
        "published": "2022-05-19T08:14:19Z",
        "url": "https://gnusocial.net/notice/11810471",
        "attributedTo": "https://gnusocial.net/index.php/user/180958",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://khp.ignorelist.com/index.php/user/1"
        ],
        "cc": [
          "https://gnusocial.net/user/180958/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6852789",
        "conversationUrl": "https://gnusocial.net/conversation/6852789#notice-11810471",
        "content": "La invasión de los ladrones de cuerpos. <br />\n<a href=\"https://es.wikipedia.org/wiki/Invasion_of_the_Body_Snatchers_(pel%C3%ADcula_de_1956)\" title=\"https://es.wikipedia.org/wiki/Invasion_of_the_Body_Snatchers_(pel%C3%ADcula_de_1956)\" rel=\"nofollow noreferrer\" class=\"attachment\">https://es.wikipedia.org/wiki/Invasion_of_the_Body_Snatchers_(pel%C3%ADcula_de_1956)</a><br />\n<br />\nNunca se ha valorado lo suficiente esta película.",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "text/html; charset=UTF-8",
            "url": "https://es.wikipedia.org/wiki/Invasion_of_the_Body_Snatchers_(pel%C3%ADcula_de_1956)",
            "size": 0,
            "name": "Invasion of the Body Snatchers (película de 1956)"
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://khp.ignorelist.com/index.php/user/1",
            "name": "aab@khp.ignorelist.com"
          }
        ],
        "inReplyTo": "https://khp.ignorelist.com/notice/1390680"
      }
    },
    {
      "created": "2022-05-17 20:47:10",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11797335",
        "type": "Note",
        "published": "2022-05-17T20:44:54Z",
        "url": "https://gnusocial.net/notice/11797335",
        "attributedTo": "https://gnusocial.net/index.php/user/68775",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}",
          "https://gnusocial.net/group/847/id"
        ],
        "cc": [
          "https://gnusocial.net/user/68775/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6849247",
        "conversationUrl": "https://gnusocial.net/conversation/6849247#notice-11797335",
        "content": "#<span class=\"tag\"><a href=\"https://gnusocial.net/tag/gnusocial\" rel=\"tag\">GNUsocial</a></span> mola aun, que cojones.<br />\n<br />\n!<a href=\"https://gnusocial.net/group/847/id\" class=\"h-card u-url p-nickname group\" title=\"Time Zone Apropiate Farewell (tzaf)\">tzaf</a>  :-)",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "gnusocial",
            "url": "https://gnusocial.net/tag/gnusocial"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://gnusocial.net/group/847/id",
            "name": "tzaf@gnusocial.net"
          }
        ]
      }
    },
    {
      "created": "2022-05-17 11:46:07",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11791027",
        "type": "Note",
        "published": "2022-05-17T09:27:30Z",
        "url": "https://gnusocial.net/notice/11791027",
        "attributedTo": "https://gnusocial.net/index.php/user/32192",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://gnusocial.net/group/774/id",
          "https://gnusocial.net/group/835/id"
        ],
        "cc": [
          "https://gnusocial.net/user/32192/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6846373",
        "conversationUrl": "https://gnusocial.net/conversation/6846373#notice-11791027",
        "content": "Este jueves 19 de mayo te esperamos en una nueva acción de Stop Desahucios Granada 15M, a las 10:30h en Plaza Isabel la Católica -Colón-<br />\n<br />\n- Porque es necesario que las familias afectadas por las malas prácticas de esta entidad, puedan permanecer en sus viviendas.<br />\n- Porque necesitamos que el derecho a la vivienda se blinde ante todos los demás intereses especulativos.<br />\n- Porque no toleramos más familias a la calle.<br />\n- Porque queremos una solución para las familias afectadas.<br />\n<br />\n<a href=\"https://afectadosporlahipotecagranada.com/jueves-19-de-mayo-en-colon/\" title=\"https://afectadosporlahipotecagranada.com/jueves-19-de-mayo-en-colon/\" rel=\"nofollow noreferrer\" class=\"attachment\">https://afectadosporlahipotecagranada.com/jueves-19-de-mayo-en-colon/</a><br />\n<br />\n!<a href=\"https://gnusocial.net/group/774/id\" class=\"h-card u-url p-nickname group\" title=\"Medios Libres (medioslibres)\">medioslibres</a> !<a href=\"https://gnusocial.net/group/835/id\" class=\"h-card u-url p-nickname group\" title=\"radioslibres (radioslibres)\">radioslibres</a> <a href=\"https://gnusocial.net/url/9466901\" title=\"https://gnusocial.net/url/9466901\" rel=\"nofollow noreferrer\" class=\"attachment\">https://gnusocial.net/url/9466901</a>",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "image/webp",
            "url": "https://gnusocial.net/attachment/a19d0c0f6e3f9dcac152c8ceac83a8c70ab43ada4710388919c4dad5b431b3e0/view",
            "size": 324664,
            "name": "accion-19-de-mayo.webp",
            "meta": {
              "width": "1240",
              "height": "1754"
            }
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://gnusocial.net/group/774/id",
            "name": "medioslibres@gnusocial.net"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://gnusocial.net/group/835/id",
            "name": "radioslibres@gnusocial.net"
          }
        ]
      }
    },
    {
      "created": "2022-05-17 11:46:04",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11790963",
        "type": "Note",
        "published": "2022-05-17T09:17:01Z",
        "url": "https://gnusocial.net/notice/11790963",
        "attributedTo": "https://gnusocial.net/index.php/user/32192",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://gnusocial.net/group/774/id",
          "https://gnusocial.net/group/835/id"
        ],
        "cc": [
          "https://gnusocial.net/user/32192/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6846337",
        "conversationUrl": "https://gnusocial.net/conversation/6846337#notice-11790963",
        "content": "Presentación del libro: Metropolice. Seguridad y policía en la ciudad neoliberal<br />\n<br />\nEsta obra colectiva se centra en el análisis de la policía o lo policial, no solo como un dispositivo fundamental para que los Estados garanticen el funcionamiento del neoliberalismo en las ciudades, sino además, cómo las políticas públicas, incluyendo los servicios sociales, la salud o la educación han terminado por emular el modo de funcionamiento policial.<br />\n<br />\nContaremos con uno de sus autores, Sergio García (Observatorio Mteropolitano de Madrid y Carabancheleando) y le acompañan Ariana S. Cota y Juan Rodríguez (Grupo de Estudios Antropológicos La Corrala).<br />\n<br />\n<a href=\"https://agenda.radioalmaina.org/evento/3395\" title=\"https://agenda.radioalmaina.org/evento/3395\" rel=\"nofollow noreferrer\" class=\"attachment\">https://agenda.radioalmaina.org/evento/3395</a><br />\n<br />\n!<a href=\"https://gnusocial.net/group/774/id\" class=\"h-card u-url p-nickname group\" title=\"Medios Libres (medioslibres)\">medioslibres</a> !<a href=\"https://gnusocial.net/group/835/id\" class=\"h-card u-url p-nickname group\" title=\"radioslibres (radioslibres)\">radioslibres</a> <a href=\"https://gnusocial.net/url/9466835\" title=\"https://gnusocial.net/url/9466835\" rel=\"nofollow noreferrer\" class=\"attachment\">https://gnusocial.net/url/9466835</a>",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "image/webp",
            "url": "https://gnusocial.net/attachment/53f4795a8df2e2dfb4792742e9f524b9be9f45884e0e7534424369b828f35660/view",
            "size": 151904,
            "name": "resized-bc415bf93dbd1e747592fb",
            "meta": {
              "width": "576",
              "height": "576"
            }
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://gnusocial.net/group/774/id",
            "name": "medioslibres@gnusocial.net"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://gnusocial.net/group/835/id",
            "name": "radioslibres@gnusocial.net"
          }
        ]
      }
    },
    {
      "created": "2022-05-12 16:53:12",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11737286",
        "type": "Note",
        "published": "2022-05-12T15:00:57Z",
        "url": "https://mastodon.social/@sursiendo/108289566638219182",
        "attributedTo": "https://mastodon.social/users/sursiendo",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/78265/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6822370",
        "conversationUrl": "https://gnusocial.net/conversation/6822370#notice-11737286",
        "content": "<p>¡11 años de Sursiendo! 🎉🥳<br />Gracias a cada una de las personas que nos acompañan en este camino de subidas y bajadas, pero sobre todo de presencia y cariño por lo que hacemos 💫</p><p><a href=\"https://mastodon.social/tags/DerechosDigitales\" class=\"mention hashtag\" rel=\"tag\">#DerechosDigitales</a><br /><a href=\"https://mastodon.social/tags/Comunalidad\" class=\"mention hashtag\" rel=\"tag\">#Comunalidad</a><br /><a href=\"https://mastodon.social/tags/Hackfeminismo\" class=\"mention hashtag\" rel=\"tag\">#Hackfeminismo</a></p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "comunalidad",
            "url": "https://gnusocial.net/tag/comunalidad"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "hackfeminismo",
            "url": "https://gnusocial.net/tag/hackfeminismo"
          }
        ]
      }
    },
    {
      "created": "2022-05-11 12:15:18",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11720900",
        "type": "Note",
        "published": "2022-05-11T12:09:55Z",
        "url": "https://gnusocial.net/notice/11720900",
        "attributedTo": "https://gnusocial.net/index.php/user/176220",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/176220/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6815351",
        "conversationUrl": "https://gnusocial.net/conversation/6815351#notice-11720900",
        "content": "Swift and easy :-)",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11720839"
      }
    },
    {
      "created": "2022-05-11 08:48:54",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11718786",
        "type": "Note",
        "published": "2022-05-11T08:45:15Z",
        "url": "https://gnusocial.net/notice/11718786",
        "attributedTo": "https://gnusocial.net/index.php/user/17097",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/17097/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6814682",
        "conversationUrl": "https://gnusocial.net/conversation/6814682#notice-11718786",
        "content": "¡Nuevo Menos Lobos 54! repasamos las últimas noticias sobre represión, hacemos una entrevista a Paula de Fridays For Future Granada sobre #<span class=\"tag\"><a href=\"https://gnusocial.net/tag/legalizalatiza\" rel=\"tag\">LegalizaLaTiza</a></span>, entrevistamos a Hector González Pérez sobre el conflicto de La Suiza y después consejos legales sobre el derecho de huelga<br />\n<a href=\"https://www.menoslobos.net/2022/05/programa-54-represion-sindical-la-suiza/\" title=\"https://www.menoslobos.net/2022/05/programa-54-represion-sindical-la-suiza/\" rel=\"nofollow noreferrer\" class=\"attachment\">https://www.menoslobos.net/2022/05/programa-54-represion-sindical-la-suiza/</a>",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "text/html; charset=UTF-8",
            "url": "https://www.menoslobos.net/2022/05/programa-54-represion-sindical-la-suiza/",
            "size": 0,
            "name": "Programa 54: Represión sindical: La Suiza - Menos Lobos"
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "legalizalatiza",
            "url": "https://gnusocial.net/tag/legalizalatiza"
          }
        ]
      }
    },
    {
      "created": "2022-05-10 19:46:04",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11712634",
        "type": "Note",
        "published": "2022-05-10T18:52:53Z",
        "url": "https://polyglot.city/@Stoori/108278963387839465",
        "attributedTo": "https://polyglot.city/users/Stoori",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://mastodon.social/users/fasnix"
        ],
        "cc": [
          "https://gnusocial.net/user/80307/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6811958",
        "conversationUrl": "https://gnusocial.net/conversation/6811958#notice-11712634",
        "content": "<p><a href=\"https://mastodon.social/@fasnix\" class=\"u-url mention\">@fasnix</a> every company doing that should run their own instance (or pool their resources in a common company instance), not come and use the hobbyists' and activists' resources.</p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mastodon.social/users/fasnix",
            "name": "fasnix@mastodon.social"
          }
        ],
        "inReplyTo": "https://mastodon.social/@fasnix/108278933358223134"
      }
    },
    {
      "created": "2022-05-10 10:21:09",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11707417",
        "type": "Note",
        "published": "2022-05-10T09:55:38Z",
        "url": "https://mastodont.cat/@spla/108277041533848690",
        "attributedTo": "https://mastodont.cat/users/spla",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/7747/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6809609",
        "conversationUrl": "https://gnusocial.net/conversation/6809609#notice-11707417",
        "content": "<p><a href=\"https://gnusocial.net/administrator\" class=\"u-url mention\">@administrator</a> si, a eso. Empecé a mirar otros proyectos y al ver que no tenían esa info me centré sólo en Mastodon.<br />Hay otros que sí la tienen pero la API no está en /nodeinfo/2.0.json (como pasa en GNUSocial), cosa que complica el código...</p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11707239"
      }
    },
    {
      "created": "2022-05-10 09:09:29",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11706985",
        "type": "Note",
        "published": "2022-05-10T08:43:17Z",
        "url": "https://gnusocial.net/notice/11706985",
        "attributedTo": "https://gnusocial.net/index.php/user/6680",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/6680/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6809435",
        "conversationUrl": "https://gnusocial.net/conversation/6809435#notice-11706985",
        "content": "jejejej eso es de novata. Copie y pegue del correo. gracias por estar siempre querida",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11706897"
      }
    },
    {
      "created": "2022-05-07 12:54:27",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11680817",
        "type": "Note",
        "published": "2022-05-07T12:52:51Z",
        "url": "https://mastodon.social/@dansup/108260751418633600",
        "attributedTo": "https://mastodon.social/users/dansup",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/70408/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6797639",
        "conversationUrl": "https://gnusocial.net/conversation/6797639#notice-11680817",
        "content": "<p>I've been pretty vocal about using my following and resources to help spread awareness of new projects and developers, I know what it's like to start from nothing and \"compete\" with mastodon, peertube, pleroma, etc.</p><p>What I don't get is how distant projects are from each other, I think the larger projects should make more of an effort to help out smaller projects.</p><p>We are not competing with each other, we're all in this together! 💪  <a href=\"https://mastodon.social/tags/fediverse\" class=\"mention hashtag\" rel=\"tag\">#fediverse</a> <a href=\"https://mastodon.social/tags/activitypub\" class=\"mention hashtag\" rel=\"tag\">#activitypub</a></p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "activitypub",
            "url": "https://gnusocial.net/tag/activitypub"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "fediverse",
            "url": "https://gnusocial.net/tag/fediverse"
          }
        ]
      }
    },
    {
      "created": "2022-05-06 12:00:59",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11669941",
        "type": "Note",
        "published": "2022-05-06T11:59:40Z",
        "url": "https://gnusocial.net/notice/11669941",
        "attributedTo": "https://gnusocial.net/index.php/user/176220",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/176220/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6754933",
        "conversationUrl": "https://gnusocial.net/conversation/6754933#notice-11669941",
        "content": "You did, and although we disagree on that specific one, I'll keep it in mind.",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11667883"
      }
    },
    {
      "created": "2022-05-06 12:00:51",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11669926",
        "type": "Note",
        "published": "2022-05-06T11:58:35Z",
        "url": "https://gnusocial.net/notice/11669926",
        "attributedTo": "https://gnusocial.net/index.php/user/176220",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/176220/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6791909",
        "conversationUrl": "https://gnusocial.net/conversation/6791909#notice-11669926",
        "content": "Great you could could do something about it. Thank you!",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11668132"
      }
    },
    {
      "created": "2022-05-06 11:41:33",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11669674",
        "type": "Note",
        "published": "2022-05-06T11:33:52Z",
        "url": "https://gnusocial.net/notice/11669674",
        "attributedTo": "https://chaos.social/users/greenfediverse",
        "to": [
          "${id}",
          "https://fediverse.one/profile/fo"
        ],
        "cc": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "conversationId": "https://gnusocial.net/conversation/6792094",
        "conversationUrl": "https://gnusocial.net/conversation/6792094#notice-11669674",
        "content": "<p><a href=\"https://gnusocial.net/administrator\" class=\"u-url mention\">@administrator</a> <br />your instance is not listed in the .json, <a href=\"https://nodes.fediverse.party/nodes.json\" rel=\"nofollow noreferrer\">https://nodes.fediverse.party/nodes.json</a>, but you are listed on <a href=\"https://fediverse.observer/search?query=gnusocial.net\" rel=\"nofollow noreferrer\">https://fediverse.observer/search?query=gnusocial.net</a> <a href=\"https://fediverse.one/profile/fo\" class=\"u-url mention\">@fo</a> when we'll use this database too, then you'll be visible again. but first we are facing some other issues ;)</p>",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "application/json",
            "url": "https://nodes.fediverse.party/nodes.json",
            "size": 157164,
            "name": "Untitled attachment"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "text/html; charset=UTF-8",
            "url": "https://fediverse.observer/search?query=gnusocial.net",
            "size": 0,
            "name": "Fediverse Observer checks all servers in the fediverse and gives you an easy way to find a home using a map or list."
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://fediverse.one/profile/fo",
            "name": "fo@fediverse.one"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11669250"
      }
    },
    {
      "created": "2022-05-06 07:48:58",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11667069",
        "type": "Note",
        "published": "2022-05-06T06:17:14Z",
        "url": "https://gnusocial.net/notice/11667069",
        "attributedTo": "https://gnusocial.net/index.php/user/10503",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}",
          "https://gnusocial.net/index.php/user/279"
        ],
        "cc": [
          "https://gnusocial.net/user/10503/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6791419",
        "conversationUrl": "https://gnusocial.net/conversation/6791419#notice-11667069",
        "content": "Apoyemos a @<a href=\"https://gnusocial.net/index.php/user/279\" class=\"h-card u-url p-nickname mention\" title=\"elbinario\">elbinario</a> y a @<a href=\"${id}\" class=\"h-card u-url p-nickname mention\" title=\"admin de gnusocial.net\">administrator</a> a mantener viva esta hermosa comunidad que es GNU Social.<br />\n<br />\nDonativos en <a href=\"https://elbinario.net/limosna-2/\" title=\"https://elbinario.net/limosna-2/\" rel=\"nofollow noreferrer\" class=\"attachment\">https://elbinario.net/limosna-2/</a> <a href=\"https://gnusocial.net/url/9355530\" title=\"https://gnusocial.net/url/9355530\" rel=\"nofollow noreferrer\" class=\"attachment thumbnail\">https://gnusocial.net/url/9355530</a>",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "image/webp",
            "url": "https://gnusocial.net/attachment/a2ad138dd146f1df384ea36cb5249b643cefd2e7a620b77f88fa3f0c4ca36db7/view",
            "size": 76036,
            "name": "DonaGNU.webp",
            "meta": {
              "width": "1021",
              "height": "680"
            }
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://gnusocial.net/index.php/user/279",
            "name": "elbinario@gnusocial.net"
          }
        ]
      }
    },
    {
      "created": "2022-05-05 19:21:05",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11662081",
        "type": "Note",
        "published": "2022-05-05T18:45:18Z",
        "url": "https://mastodon.uy/@jorge/108250812574686830",
        "attributedTo": "https://mastodon.uy/users/jorge",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/98861/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6789276",
        "conversationUrl": "https://gnusocial.net/conversation/6789276#notice-11662081",
        "content": "<p>NFT: No future tenemos</p><p>*Version spanglish</p>",
        "isLocal": false,
        "attachment": [],
        "tag": []
      }
    },
    {
      "created": "2022-05-05 08:44:52",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11653321",
        "type": "Note",
        "published": "2022-05-05T02:09:08Z",
        "url": "https://gnusocial.net/notice/11653321",
        "attributedTo": "https://gnusocial.net/index.php/user/10503",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://gnusocial.net/index.php/user/279"
        ],
        "cc": [
          "https://gnusocial.net/user/10503/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6785477",
        "conversationUrl": "https://gnusocial.net/conversation/6785477#notice-11653321",
        "content": "Apoyemos a @<a href=\"https://gnusocial.net/index.php/user/279\" class=\"h-card u-url p-nickname mention\" title=\"elbinario\">elbinario</a> y a esta hermosa comunidad que es GNU Social  <br />\n<a href=\"https://elbinario.net/limosna-2/\" title=\"https://elbinario.net/limosna-2/\" rel=\"nofollow external noreferrer\" class=\"attachment\" id=\"attachment-8367471\">https://elbinario.net/limosna-2/</a>",
        "isLocal": true,
        "attachment": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Document",
            "mediaType": "text/html; charset=UTF-8",
            "url": "https://elbinario.net/limosna-2/",
            "size": 0,
            "name": "Donaciones – Elbinario"
          }
        ],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://gnusocial.net/index.php/user/279",
            "name": "elbinario@gnusocial.net"
          }
        ]
      }
    },
    {
      "created": "2022-05-04 18:06:37",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11648593",
        "type": "Note",
        "published": "2022-05-04T17:52:13Z",
        "url": "https://xarxa.cloud/@t3rr0rz0n3/108244941654991242",
        "attributedTo": "https://xarxa.cloud/users/t3rr0rz0n3",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/75578/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6783603",
        "conversationUrl": "https://gnusocial.net/conversation/6783603#notice-11648593",
        "content": "<p>Pels nous he preparat això! <a href=\"https://faqs.xarxa.cloud/\" rel=\"nofollow noreferrer\">https://faqs.xarxa.cloud/</a> Està en procés, pero vaig fent i està quedant prou bé!</p>",
        "isLocal": false,
        "attachment": [],
        "tag": []
      }
    },
    {
      "created": "2022-04-28 11:31:55",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11564928",
        "type": "Note",
        "published": "2022-04-28T11:26:24Z",
        "url": "https://gnusocial.net/notice/11564928",
        "attributedTo": "https://gnusocial.net/index.php/user/176220",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/176220/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6748225",
        "conversationUrl": "https://gnusocial.net/conversation/6748225#notice-11564928",
        "content": "When stigatle acquired the instance he started running it from Norway.",
        "isLocal": true,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11564813"
      }
    },
    {
      "created": "2022-04-28 10:19:06",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11563708",
        "type": "Note",
        "published": "2022-04-28T09:57:59Z",
        "url": "https://pleroma.debian.social/objects/b66af697-9d31-495a-9267-c6f7557009e9",
        "attributedTo": "https://pleroma.debian.social/users/highvoltage",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/118433/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6748165",
        "conversationUrl": "https://gnusocial.net/conversation/6748165#notice-11563708",
        "content": "Nice to see a real-life Debian meeting again! Here's a blog post from pollo who organised a meet-up in Montréal: <a href=\"https://veronneau.org/montreals-debian-stuff-april-2022.html\">https://veronneau.org/montreals-debian-stuff-april-2022.html</a>",
        "isLocal": false,
        "attachment": [],
        "tag": []
      }
    },
    {
      "created": "2022-04-27 19:19:12",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11554648",
        "type": "Note",
        "published": "2022-04-27T19:13:38Z",
        "url": "https://hostux.social/@valere/108205625393751622",
        "attributedTo": "https://hostux.social/users/valere",
        "to": [
          "https://mstdn.social/users/stux"
        ],
        "cc": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "conversationId": "https://gnusocial.net/conversation/6744503",
        "conversationUrl": "https://gnusocial.net/conversation/6744503#notice-11554648",
        "content": "<p><a href=\"https://mstdn.social/@stux\" class=\"u-url mention\">@stux</a> consider closing registrations, you don't have to put yourself at risk financially and grow your instance indefinitely. There are enough instances to absorb these waves and centralization is not good for the fediverse</p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mstdn.social/users/stux",
            "name": "stux@mstdn.social"
          }
        ],
        "inReplyTo": "https://mstdn.social/@stux/108205582667086490"
      }
    },
    {
      "created": "2022-04-26 22:28:28",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11539207",
        "type": "Note",
        "published": "2022-04-26T22:22:46Z",
        "url": "https://scholar.social/@louce/108200682479725296",
        "attributedTo": "https://scholar.social/users/louce",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/211376/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6738383",
        "conversationUrl": "https://gnusocial.net/conversation/6738383#notice-11539207",
        "content": "<p>I do hope this en masse migration will be long lasting and not reverse to the same old social media platforms. Twitter has been a relentless neoliberal tool that has made social media self-PR a twisted (and unpaid) part of the job for many academics. Performative, competitive &amp; exploitative. I don’t want to be part of that. I want to share ideas, info &amp; research that folks might find useful &amp; interesting. Open knowledge 🔓</p>",
        "isLocal": false,
        "attachment": [],
        "tag": []
      }
    },
    {
      "created": "2022-04-26 09:03:51",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11525768",
        "type": "Note",
        "published": "2022-04-26T08:59:15Z",
        "url": "https://hispagatos.space/@moribundo/108197547484809198",
        "attributedTo": "https://hispagatos.space/users/moribundo",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "https://gnusocial.net/user/194962/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6732789",
        "conversationUrl": "https://gnusocial.net/conversation/6732789#notice-11525768",
        "content": "<p>Se recuerda a la gente que llega nueva que el Fediverso (y Mastodon) es software libre, que no hay una empresa millonaria detrás traficando con tus datos, y que muchos nodos están siendo alojados por personas en sus casas, pagando ellos los gastos para que podamos disfrutar de ello, así que las donaciones económicas a estos proyectos libres son muy bienvenidas, por pequeñas que sean.<br />Págale una birra o un café al desarrollador o administrador de vez en cuando para su sustento, lo agradecerá.</p>",
        "isLocal": false,
        "attachment": [],
        "tag": []
      }
    },
    {
      "created": "2022-04-25 20:38:39",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11517791",
        "type": "Note",
        "published": "2022-04-25T20:22:00Z",
        "url": "https://hackers.town/@lmorchard/108194529166052164",
        "attributedTo": "https://hackers.town/users/lmorchard",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/137885/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6728950",
        "conversationUrl": "https://gnusocial.net/conversation/6728950#notice-11517791",
        "content": "<p><a href=\"https://gnusocial.net/administrator\" class=\"u-url mention\" rel=\"nofollow noreferrer\">@administrator</a> Yeah, that bit's ironic for me anyway</p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11517402"
      }
    },
    {
      "created": "2022-04-25 20:02:54",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11517202",
        "type": "Note",
        "published": "2022-04-25T19:55:33Z",
        "url": "https://hackers.town/@lmorchard/108194399465423638",
        "attributedTo": "https://hackers.town/users/lmorchard",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://hackers.town/users/thegibson",
          "https://freeradical.zone/users/tek"
        ],
        "cc": [
          "https://gnusocial.net/user/137885/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6728950",
        "conversationUrl": "https://gnusocial.net/conversation/6728950#notice-11517202",
        "content": "<p><a href=\"https://hackers.town/@thegibson\" class=\"u-url mention\">@thegibson</a> <a href=\"https://freeradical.zone/@tek\" class=\"u-url mention\" rel=\"nofollow noreferrer\">@tek</a> This gif I found is probably from somewhere terrible, but it's kind of how I picture server admins</p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://hackers.town/users/thegibson",
            "name": "thegibson@hackers.town"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://freeradical.zone/users/tek",
            "name": "tek@freeradical.zone"
          }
        ],
        "inReplyTo": "https://hackers.town/@thegibson/108194382641115818"
      }
    },
    {
      "created": "2022-04-25 16:14:38",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11513441",
        "type": "Note",
        "published": "2022-04-25T16:12:18Z",
        "url": "https://hispagatos.space/@moribundo/108193588033174439",
        "attributedTo": "https://hispagatos.space/users/moribundo",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://mastodon.social/users/durru",
          "https://mstdn.social/users/pela0yakuza"
        ],
        "cc": [
          "https://gnusocial.net/user/194962/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6727390",
        "conversationUrl": "https://gnusocial.net/conversation/6727390#notice-11513441",
        "content": "<p><a href=\"https://mstdn.social/@pela0yakuza\" class=\"u-url mention\">@pela0yakuza</a><br />Jajajajaja. Para mi mastodon siempre será mastodon.social, igual que gnusocial siempre será gnusocial.net<br /><a href=\"https://mastodon.social/@durru\" class=\"u-url mention\">@durru</a></p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mastodon.social/users/durru",
            "name": "durru@mastodon.social"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mstdn.social/users/pela0yakuza",
            "name": "pela0yakuza@mstdn.social"
          }
        ]
      }
    },
    {
      "created": "2022-04-18 19:01:34",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11450150",
        "type": "Note",
        "published": "2022-04-18T16:58:02Z",
        "url": "https://masto.nobigtech.es/@rafapoverello/108154131662775365",
        "attributedTo": "https://masto.nobigtech.es/users/rafapoverello",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://masto.rocks/users/aperalesf"
        ],
        "cc": [
          "https://gnusocial.net/user/195881/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6696034",
        "conversationUrl": "https://gnusocial.net/conversation/6696034#notice-11450150",
        "content": "<p><a href=\"https://masto.rocks/@aperalesf\" class=\"u-url mention\">@aperalesf</a> Supuestamente, al haber más usuaries, hay más posibilidad de interacción, pero en GNUsocial siempre he podido debatir también sobre anarquismo, veganismo, feminismo, y hasta fricadas de cómics que ya están por aquí de lo más perdidos.<br />Y, por cierto, GNUsocial todavía sigue vivo como parte del <a href=\"https://masto.nobigtech.es/tags/fediverso\" class=\"mention hashtag\" rel=\"tag\">#fediverso</a> gracias al curro y al esfuerzo de admins de algunas instancias que se negaron a arrojar la toalla :-).</p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "name": "fediverso",
            "url": "https://gnusocial.net/tag/fediverso"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://masto.rocks/users/aperalesf",
            "name": "aperalesf@masto.rocks"
          }
        ],
        "inReplyTo": "https://masto.rocks/@aperalesf/108153952377258174"
      }
    },
    {
      "created": "2022-04-15 19:58:21",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11425791",
        "type": "Note",
        "published": "2022-04-15T19:50:15Z",
        "url": "https://libranet.de/display/0b6b25a8-1362-59cc-324a-998212423617",
        "attributedTo": "https://libranet.de/profile/miguel",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://mastodon.la/users/void"
        ],
        "cc": [
          "https://gnusocial.net/user/75208/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6679574",
        "conversationUrl": "https://gnusocial.net/conversation/6679574#notice-11425791",
        "content": "Excelente iniciativa, <a href=\"https://mastodon.la/users/void\" class=\"u-url mention\">@void</a>. Sigamos alimentándola entre todes.",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mastodon.la/users/void",
            "name": "void@mastodon.la"
          }
        ],
        "inReplyTo": "https://mastodon.la/@void/108131583988218264"
      }
    },
    {
      "created": "2022-04-13 12:31:04",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11405421",
        "type": "Note",
        "published": "2022-04-13T11:59:33Z",
        "url": "https://mastodon.sdf.org/@ghostdancer/108124646413458014",
        "attributedTo": "https://mastodon.sdf.org/users/ghostdancer",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "https://mastodon.social/users/CorioPsicologia",
          "https://mastodon.madrid/users/fanta",
          "https://mastodon.madrid/users/puppetmaster",
          "https://masto.nobigtech.es/users/niko"
        ],
        "cc": [
          "https://gnusocial.net/user/78302/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6673741",
        "conversationUrl": "https://gnusocial.net/conversation/6673741#notice-11405421",
        "content": "<p><a href=\"https://masto.nobigtech.es/@niko\" class=\"u-url mention\">@niko</a> Las redes monetizadas necesitan enganche para ser rentables y eso se consigue con posturas extremas o polémicas por eso son el caldo de cultivo para individuos de ese tipo. Es el siguiente paso en la evolución de los personajes que se han visto en los realities, ahora ya no necesitan pasar un casting, tienen las herramientas en casa y audiencias todavía mayores y sin horarios fijos. Y los seres humanos somos así de manipulables, nos va el morbo. <a href=\"https://mastodon.madrid/@puppetmaster\" class=\"u-url mention\">@puppetmaster</a> <a href=\"https://mastodon.madrid/@fanta\" class=\"u-url mention\">@fanta</a> <a href=\"https://mastodon.social/@CorioPsicologia\" class=\"u-url mention\">@CorioPsicologia</a></p>",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mastodon.social/users/CorioPsicologia",
            "name": "CorioPsicologia@mastodon.social"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mastodon.madrid/users/fanta",
            "name": "fanta@mastodon.madrid"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://mastodon.madrid/users/puppetmaster",
            "name": "puppetmaster@mastodon.madrid"
          },
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "https://masto.nobigtech.es/users/niko",
            "name": "niko@masto.nobigtech.es"
          }
        ]
      }
    },
    {
      "created": "2022-04-13 07:51:20",
      "object": {
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": "https://gnusocial.net/object/note/11402830",
        "type": "Note",
        "published": "2022-04-13T01:35:49Z",
        "url": "https://nu.federati.net/notice/3397662",
        "attributedTo": "https://nu.federati.net/user/2",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public",
          "${id}"
        ],
        "cc": [
          "https://gnusocial.net/user/4069/followers.json"
        ],
        "conversationId": "https://gnusocial.net/conversation/6671946",
        "conversationUrl": "https://gnusocial.net/conversation/6671946#notice-11402830",
        "content": "My last incoming DM was 2020-07-03. Some update after that seems to have broken them, even on the same server.",
        "isLocal": false,
        "attachment": [],
        "tag": [
          {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Mention",
            "href": "${id}",
            "name": "administrator@gnusocial.net"
          }
        ],
        "inReplyTo": "https://gnusocial.net/activity/11400893"
      }
    }
  ]
}
